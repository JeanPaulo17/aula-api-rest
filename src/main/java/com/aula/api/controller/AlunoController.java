package com.aula.api.controller;

import com.aula.api.domain.Aluno;
import com.aula.api.service.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("aluno")
public class AlunoController {

    @Autowired
    private AlunoService alunoService;

    @GetMapping()
    List<Aluno> findAll() {
        return alunoService.findAll();
    }

    @GetMapping("/{id}")
    ResponseEntity<Aluno> findById(@PathVariable Long id) {
       Optional<Aluno> aluno = alunoService.findById(id);
       if(aluno.isPresent()){
           return ResponseEntity.ok(alunoService.findById(id).get());
       }else{
           return ResponseEntity.notFound().build();
       }
    }

    @PostMapping()
    ResponseEntity<Aluno> create(@RequestBody Aluno aluno, UriComponentsBuilder uriBuilder) throws Exception {
        Aluno novoAluno = alunoService.create(aluno);
        URI uri = uriBuilder.path("/aluno/{id}").buildAndExpand(novoAluno.getId()).toUri();
        return ResponseEntity.created(uri).body(novoAluno);
    }

    @PutMapping()
    ResponseEntity<Aluno> update(@RequestBody Aluno aluno){
        return ResponseEntity.ok(alunoService.update(aluno));
    }

    @DeleteMapping("{id}")
    ResponseEntity deleteById(@PathVariable Long id){
        Optional<Aluno> aluno = alunoService.findById(id);
        if (aluno.isPresent()){
            alunoService.deleteById(id);
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }
}
