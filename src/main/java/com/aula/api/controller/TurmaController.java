package com.aula.api.controller;

import com.aula.api.domain.Aluno;
import com.aula.api.domain.Turma;
import com.aula.api.service.TurmaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("turma")
public class TurmaController {

    @Autowired
    private TurmaService turmaService;

    @GetMapping()
    List<Turma> findAll() {
        return turmaService.findAll();
    }

    @GetMapping("/{id}")
    ResponseEntity<Turma> findById(@PathVariable Long id) {
        Optional<Turma> turma = turmaService.findById(id);
        if (turma.isPresent()){
            return ResponseEntity.ok(turma.get());
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping()
    ResponseEntity<Turma> create(@RequestBody Turma turma, UriComponentsBuilder uriBuilder){
        Turma novaTurma = turmaService.create(turma);
        URI uri = uriBuilder.path("/turma/{id}").buildAndExpand(novaTurma.getId()).toUri();
        return ResponseEntity.created(uri).body(novaTurma);
    }

    @PutMapping()
    ResponseEntity<Turma> update(@RequestBody Turma turma){
        Turma turmaNova = turmaService.update(turma);
        return ResponseEntity.ok(turmaNova);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<?> deleteById(@PathVariable Long id){
        Optional<Turma> turma = turmaService.findById(id);
        if (turma.isPresent()){
            turmaService.deleteById(id);
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/{idTurma}/aluno")
    ResponseEntity<Turma> adicionaAlunoEmTurma(@RequestBody Aluno aluno, @PathVariable() Long idTurma){
        return turmaService.adicionaAlunoEmTurma(aluno,idTurma);
    }

    @DeleteMapping("/{idTurma}/aluno")
    ResponseEntity<?> removeAlunoDeTurma(@RequestBody Aluno aluno, @PathVariable() Long idTurma){
        return turmaService.removeAlunoDeTurma(aluno,idTurma);
    }
}
