package com.aula.api.controller;

import com.aula.api.domain.Curso;
import com.aula.api.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("curso")
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @GetMapping()
    List<Curso> findAll() {
        return cursoService.findAll();
    }

    @GetMapping("/{id}")
    ResponseEntity<Curso> findById(@PathVariable Long id) {
        Optional<Curso> curso = cursoService.findById(id);
        if (curso.isPresent()){
            return ResponseEntity.ok(curso.get());
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping()
    ResponseEntity<Curso> create(@RequestBody Curso curso, UriComponentsBuilder uriBuilder){
        Curso novoCurso = cursoService.create(curso);
        URI uri = uriBuilder.path("/curso/{id}").buildAndExpand(novoCurso.getId()).toUri();
        return ResponseEntity.created(uri).body(novoCurso);
    }

    @PutMapping()
    ResponseEntity<Curso> update(@RequestBody Curso curso){
        return ResponseEntity.ok(cursoService.update(curso));
    }

    @DeleteMapping("/{id}")
    ResponseEntity<?> deleteById(@PathVariable Long id){
        Optional<Curso> curso = cursoService.findById(id);
        if (curso.isPresent()){
            cursoService.deleteById(id);
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }
}
