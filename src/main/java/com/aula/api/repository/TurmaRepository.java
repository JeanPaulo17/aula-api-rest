package com.aula.api.repository;

import com.aula.api.domain.Turma;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TurmaRepository extends CrudRepository<Turma, Long>, JpaSpecificationExecutor {

    List<Turma> findAll();
    Optional<Turma> findById(Long id);
}
