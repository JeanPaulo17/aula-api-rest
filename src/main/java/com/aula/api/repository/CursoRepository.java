package com.aula.api.repository;

import com.aula.api.domain.Curso;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CursoRepository extends CrudRepository<Curso, Long>, JpaSpecificationExecutor {

    List<Curso> findAll();
    Optional<Curso> findById(Long id);
}
