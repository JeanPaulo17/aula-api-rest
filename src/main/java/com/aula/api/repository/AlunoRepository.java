package com.aula.api.repository;

import com.aula.api.domain.Aluno;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AlunoRepository extends CrudRepository<Aluno, Long>, JpaSpecificationExecutor {

    List<Aluno> findAll();
    Optional<Aluno> findById(Long id);
}
