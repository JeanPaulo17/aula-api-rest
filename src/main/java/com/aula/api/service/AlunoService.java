package com.aula.api.service;

import com.aula.api.domain.Aluno;
import com.aula.api.dto.AlunoDTO;
import com.aula.api.repository.AlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import static javax.servlet.http.HttpServletResponse.SC_OK;

@Service
public class AlunoService {

    @Autowired
    private AlunoRepository alunoRepository;

    public List<Aluno> findAll(){
        return alunoRepository.findAll();
    }

    public Optional<Aluno> findById(Long id) {
        return alunoRepository.findById(id);
    }


    public Aluno create(Aluno aluno) throws Exception{

        RestTemplate restTemplate = new RestTemplate();

        //API FAKE - Fazendo alusão a alguma api que recebe um CEP e retorna os dados de um endereço.
        // EXEMPLO - http://api-externa.cep.correios/api/cep/25555182
        final String baseUrl = "http://www.mocky.io/v2/5e9f1ad42d00007800cb79d4";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        HttpEntity<AlunoDTO> requestEntity = new HttpEntity<>(null, headers);
        URI uri = new URI(baseUrl);

        try {
            ResponseEntity<AlunoDTO> enderecoVindoDaAPI = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, AlunoDTO.class);

            if (enderecoVindoDaAPI.getStatusCodeValue() == SC_OK){

                Aluno alunoNovo = aluno;
                alunoNovo.setLogradouro(enderecoVindoDaAPI.getBody().getLogradouro());
                alunoNovo.setBairro(enderecoVindoDaAPI.getBody().getBairro());
                alunoNovo.setCidade(enderecoVindoDaAPI.getBody().getCidade());
                alunoNovo.setNumero(enderecoVindoDaAPI.getBody().getNumero());
                alunoNovo.setCep(enderecoVindoDaAPI.getBody().getCep());
                alunoNovo.setUf(enderecoVindoDaAPI.getBody().getUf());
                alunoNovo.setPais(enderecoVindoDaAPI.getBody().getPais());

                return alunoRepository.save(alunoNovo);
            }

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return aluno;
    }

    public void deleteById(Long id) {
        alunoRepository.deleteById(id);
    }

    public Aluno update(Aluno aluno){
        Optional<Aluno> antigo = alunoRepository.findById(aluno.getId());
        antigo = Optional.of(aluno);
        return alunoRepository.save(antigo.get());
    }
}
