package com.aula.api.service;

import com.aula.api.domain.Curso;
import com.aula.api.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class CursoService {

    @Autowired
    private CursoRepository cursoRepository;

    public List<Curso> findAll(){
        return cursoRepository.findAll();
    }

    public Optional<Curso> findById(Long id){
        return cursoRepository.findById(id);
    }

    public Curso create(Curso curso){
        return cursoRepository.save(curso);
    }

    public void deleteById(Long id){
        cursoRepository.deleteById(id);
    }

    public Curso update(Curso curso){
        Optional<Curso> antigo = cursoRepository.findById(curso.getId());
        antigo = Optional.of(curso);
        return cursoRepository.save(antigo.get());
    }
}
