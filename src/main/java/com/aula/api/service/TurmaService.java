package com.aula.api.service;
import com.aula.api.domain.Aluno;
import com.aula.api.domain.Curso;
import com.aula.api.domain.Turma;
import com.aula.api.repository.TurmaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class TurmaService {

    @Autowired
    private TurmaRepository turmaRepository;

    @Autowired
    private CursoService cursoService;

    public List<Turma> findAll(){
        return turmaRepository.findAll();
    }

    public Optional<Turma> findById(Long id){
        return turmaRepository.findById(id);
    }

    public Turma create(Turma turma){
        Curso curso = cursoService.findById(turma.getCurso().getId()).get();
        turma.setCurso(curso);
        turmaRepository.save(turma);
        return turma;
    }

    public void deleteById(Long id){
        turmaRepository.deleteById(id);
    }

    public Turma update(Turma turma){
        Turma turmaNova = turmaRepository.save(turma);
        return turmaNova;
    }

    public ResponseEntity<Turma> adicionaAlunoEmTurma(Aluno aluno, Long idTurma){
        Optional<Turma> turma = turmaRepository.findById(idTurma);
        if(turma.isPresent()) {
            if(turma.get().getListaDeAlunos().contains(aluno)) {
                return ResponseEntity.badRequest().build();
            }else{
                turma.get().getListaDeAlunos().add(aluno);
                Turma turmaSalva = turmaRepository.save(turma.get());
                return ResponseEntity.ok(turmaSalva);
            }
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    public ResponseEntity removeAlunoDeTurma(Aluno aluno, Long idTurma){
        Optional<Turma> turma = turmaRepository.findById(idTurma);
        if(turma.isPresent() && turma.get().getListaDeAlunos().contains(aluno)) {
            for (Aluno alunoDaLista : turma.get().getListaDeAlunos()) {
                if (alunoDaLista.equals(aluno)) {
                    turma.get().getListaDeAlunos().remove(aluno);
                    turmaRepository.save(turma.get());
                    return ResponseEntity.ok().build();
                }
            }
        }else {
            return ResponseEntity.notFound().build();
        }
        return null;
    }
}
