package com.aula.api.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Turma {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Column
    @Getter
    @Setter
    private String codigo;

    @ManyToOne(cascade = CascadeType.ALL)
    @Getter
    @Setter
    private Curso curso;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name="turma_aluno", joinColumns={@JoinColumn(name="id_turma")}, inverseJoinColumns={@JoinColumn(name="id_aluno")})
    @Getter
    @Setter
    private List<Aluno> listaDeAlunos = new ArrayList();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Turma turma = (Turma) o;
        return Objects.equals(id, turma.id) &&
                Objects.equals(codigo, turma.codigo) &&
                Objects.equals(curso, turma.curso);
    }
}
