package com.aula.api.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Aluno {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Column
    @Getter
    @Setter
    private String nome;

    @Column
    @Getter
    @Setter
    private String sobrenome;

    @Column
    @Getter
    @Setter
    private String matricula;

    @Column
    @Getter
    @Setter
    private Integer idade;

    @Column
    @Getter
    @Setter
    private String logradouro;

    @Column
    @Getter
    @Setter
    private String cidade;

    @Column
    @Getter
    @Setter
    private String bairro;

    @Column
    @Getter
    @Setter
    private Integer numero;

    @Column
    @Getter
    @Setter
    private String uf;

    @Column
    @Getter
    @Setter
    private String cep;

    @Column
    @Getter
    @Setter
    private String pais;

    @ManyToMany(mappedBy = "listaDeAlunos")
    @Getter
    @Setter
    @JsonBackReference
    private List<Turma> listaDeTurmas;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aluno aluno = (Aluno) o;
        return Objects.equals(id, aluno.id) &&
                Objects.equals(nome, aluno.nome) &&
                Objects.equals(sobrenome, aluno.sobrenome) &&
                Objects.equals(matricula, aluno.matricula) &&
                Objects.equals(idade, aluno.idade);
    }
}
