package com.aula.api.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Curso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Column
    @Getter
    @Setter
    private String codigo;

    @Column
    @Getter
    @Setter
    private String nome;
}
