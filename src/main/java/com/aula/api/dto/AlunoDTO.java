package com.aula.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

public class AlunoDTO {

    @Getter
    @Setter
    @JsonProperty("logradouro")
    private String logradouro;

    @Getter
    @Setter
    @JsonProperty("cidade")
    private String cidade;

    @Getter
    @Setter
    @JsonProperty("bairro")
    private String bairro;

    @Getter
    @Setter
    @JsonProperty("numero")
    private Integer numero;

    @Getter
    @Setter
    @JsonProperty("uf")
    private String uf;

    @Getter
    @Setter
    @JsonProperty("cep")
    private String cep;

    @Getter
    @Setter
    @JsonProperty("pais")
    private String pais;
}
